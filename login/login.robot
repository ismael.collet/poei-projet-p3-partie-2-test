*** Settings ***
Documentation     Test de la fonctionalité de log-in de opensource-demo.orangehrmlive.com
Library    SeleniumLibrary
Resource    resources/login.resources.robot
Test Setup    Load the page
Test Teardown    Close Browser

*** Test Cases ***
Valid login
    When the user types a valid username
    And the user types a valid password
    And the user pushes the login button
    Then the dashboard page shall be load
Bad username
    When the user types a wrong username
    And the user types a valid password
    And the user pushes the login button
    Then "Invalid credentials" shall be displayed
Bad password
    When the user types a valid username
    And the user types a wrong password
    And the user pushes the login button
    Then "Invalid credentials" shall be displayed
No username
    When the user types a valid password
    And the user pushes the login button
    Then "Required" shall be displayed under the username box
No password
    When the user types a valid username
    And the user pushes the login button
    Then "Required" shall be displayed under the password box
No username nor password
    When the user pushes the login button
    Then "Required" shall be displayed under the username box
    And "Required" shall be displayed under the password box

*** Keywords ***
Load the page
    Open Browser    ${url - homepage}    ${browser}
    Wait Until Page Contains Element  ${locator}[login button]
the user types a valid username
    Input text    ${locator}[username box]    ${valid username}
the user types a wrong username
    Input text    ${locator}[username box]    ${wrong username}
the user types a valid password
    Input text    ${locator}[password box]    ${valid password}
the user types a wrong password
    Input text    ${locator}[password box]    ${wrong password}
the user pushes the login button
    Click Button    ${locator}[login button]
    Sleep    ${time to wait for loading}
the dashboard page shall be load
    ${url}=    Get Location
    Should Be Equal As Strings    ${url}    ${url - dashboard}
"Invalid credentials" shall be displayed
    Check alert    ${locator}[alert invalid credentials]    
    ...    ${text}[alert invalid credentials]
"Required" shall be displayed under the username box
    Check alert    ${locator}[alert unsername required]    
    ...    ${text}[alert unsername required]
"Required" shall be displayed under the password box
    Check alert    ${locator}[alert password required]    
    ...    ${text}[alert password required]  
Check alert
    [Arguments]    ${locator}    ${expected text}
    Page Should Contain Element    ${locator}
    ${displayed text}=    Get Text    ${locator}
    Should Be Equal As Strings    ${displayed text}    ${expected text}
