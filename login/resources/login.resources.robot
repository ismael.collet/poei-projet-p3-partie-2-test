*** Settings ***
Documentation     Ressources de login.robot
Library    SeleniumLibrary
*** Variables ***
${url - homepage}    https://opensource-demo.orangehrmlive.com/web/index.php/auth/login
${url - dashboard}    https://opensource-demo.orangehrmlive.com/web/index.php/dashboard/index
${browser}    firefox
${valid username}    Admin
${wrong username}    wrong username
${valid password}    admin123
${wrong password}    wrong password
${time to wait for loading}    0.5

&{text}    alert invalid credentials=Invalid credentials
...    alert unsername required=Required
...    alert password required=Required

&{locator}    username box=name:username
...    password box=name:password
...    login button=class:orangehrm-login-button
...    alert invalid credentials=//p[@class="oxd-text oxd-text--p oxd-alert-content-text"]
...    alert unsername required=//div[@class="oxd-form-row"][1]//span[@class="oxd-text oxd-text--span oxd-input-field-error-message oxd-input-group__message"]
...    alert password required=//div[@class="oxd-form-row"][2]//span[@class="oxd-text oxd-text--span oxd-input-field-error-message oxd-input-group__message"]
