# Projet P3 - Partie 2 : Test

## Execution
Avant d'exécuter les scripts robot, il faut installer et configurer l'environnement virtuel :
```bash
# Mise en place de l'environnement virtuel
python3 -m venv .env
source .env/bin/activate

# Installation de robot framework et selenium
# Pour installer le même environnement que https://github.com/robotframework/WebDemo
pip install -r requirements.txt
```

## Scénarios de test
Les scénarios de test se trouvent dans les fichiers login/login.robot, dashboard/dashboard.robot et appointment/appointment.robot, sous la partie \*\*\*Test Cases***

### Tests de login
```RobotFramework
*** Test Cases ***
Valid login
    When the user types a valid username
    And the user types a valid password
    And the user pushes the login button
    Then the dashboard page shall be load
Bad username
    When the user types a wrong username
    And the user types a valid password
    And the user pushes the login button
    Then "Invalid credentials" shall be displayed
Bad password
    When the user types a valid username
    And the user types a wrong password
    And the user pushes the login button
    Then "Invalid credentials" shall be displayed
No username
    When the user types a valid password
    And the user pushes the login button
    Then "Required" shall be displayed under the username box
No password
    When the user types a valid username
    And the user pushes the login button
    Then "Required" shall be displayed under the password box
No username nor password
    When the user pushes the login button
    Then "Required" shall be displayed under the username box
    And "Required" shall be displayed under the password box
```

### Test du dashboard
```RobotFramework
*** Test Cases ***
Tool displaying
    The number of displayed tools shall be equal to the number of expected tools
    And all expected tools shall be displayed in the side panel menu
```

### Tests de prise de rendez-vous
```RobotFramework
*** Test Cases ***
Enter login page
    Given the home page is loaded
    When the user clicks on "Make Appointment"
    Then the user shall be on the login page
Login with valid credentials
    Given the login page is loaded
    When the user types a valid username
    And the user types a valid password
    And the user clicks on "Login"
    Then the user shall be on the appointment page
Login with bad username
    Given the login page is loaded
    When the user types a wrong username
    And the user types a valid password
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with bad password
    Given the login page is loaded
    When the user types a valid username
    And the user types a wrong password
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with no username
    Given the login page is loaded
    When the user types a valid password
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with no password
    Given the login page is loaded
    When the user types a valid username
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with no username nor password
    Given the login page is loaded
    When the user clicks on "Login"
    Then the login failure message shall appear
Appointment page display
    Given the appointment page is loaded
    Then the page shall display the facility selection scrolling menu
    and the page shall display the apply for readmission checkbox
    and the page shall display the medicare radio button
    and the page shall display the medicaid radio button
    and the page shall display the none radio button
    and the page shall display the visit date selector
    and the page shall display the comment text box
    and the page shall display the book appointment button
Make an valid appointment
    Given the appointment page is loaded
    When the user types an appointment date
    and the user clicks on "Book Appointment"
    Then the user shall be on the appointment confirmation page
Make an appointment without a date
    Given the appointment page is loaded
    and the user clicks on "Book Appointment"
    Then the user shall be on the appointment page
Logout
    Given the user has made an appointment
    When the user clicks on the lateral page toggle button
    and the user clicks on "Logout"
    Then the user shall be on the home page
```