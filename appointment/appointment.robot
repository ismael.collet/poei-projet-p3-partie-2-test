*** Settings ***
Documentation     Test dedes fonctionalités de https://katalon-demo-cura.herokuapp.com
Library    SeleniumLibrary
Resource    resources/appointment.resources.robot
Test Teardown    Close Browser

*** Test Cases ***
Enter login page
    Given the home page is loaded
    When the user clicks on "Make Appointment"
    Then the user shall be on the login page
Login with valid credentials
    Given the login page is loaded
    When the user types a valid username
    And the user types a valid password
    And the user clicks on "Login"
    Then the user shall be on the appointment page
Login with bad username
    Given the login page is loaded
    When the user types a wrong username
    And the user types a valid password
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with bad password
    Given the login page is loaded
    When the user types a valid username
    And the user types a wrong password
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with no username
    Given the login page is loaded
    When the user types a valid password
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with no password
    Given the login page is loaded
    When the user types a valid username
    And the user clicks on "Login"
    Then the login failure message shall appear
Login with no username nor password
    Given the login page is loaded
    When the user clicks on "Login"
    Then the login failure message shall appear
Appointment page display
    Given the appointment page is loaded
    Then the page shall display the facility selection scrolling menu
    and the page shall display the apply for readmission checkbox
    and the page shall display the medicare radio button
    and the page shall display the medicaid radio button
    and the page shall display the none radio button
    and the page shall display the visit date selector
    and the page shall display the comment text box
    and the page shall display the book appointment button
Make an valid appointment
    Given the appointment page is loaded
    When the user types an appointment date
    and the user clicks on "Book Appointment"
    Then the user shall be on the appointment confirmation page
Make an appointment without a date
    Given the appointment page is loaded
    and the user clicks on "Book Appointment"
    Then the user shall be on the appointment page
Logout
    Given the user has made an appointment
    When the user clicks on the lateral page toggle button
    and the user clicks on "Logout"
    Then the user shall be on the home page

*** Keywords ***
the home page is loaded
    Open Browser    ${urls}[home]    ${browser}
    Wait Until Page Contains Element  ${locators}["Make Appointment"]
the login page is loaded
    the home page is loaded
    The user clicks on "Make Appointment"
    Wait Until Page Contains Element  ${locators}["Login"]
the appointment page is loaded
    the login page is loaded
    the user types a valid username
    the user types a valid password
    the user clicks on "Login"
    Wait Until Page Contains Element  ${locators}[Appointment page title]
the user has made an appointment
    the appointment page is loaded
    the user types an appointment date
    the user clicks on "Book Appointment"
the user clicks on ${element}
    Click Element    ${locators}[${element}]
the user shall be on the ${page} page
    ${current url}=    Get Location
    Should Be Equal As Strings    ${current url}    ${urls}[${page}]
the user types a ${type} username
    Input text    ${locators}[username]    ${username}[${type}]
the user types a ${type} password
    Input text    ${locators}[password]    ${password}[${type}]
the login failure message shall appear
    Page Should Contain Element    ${locators}[login failed message]
    ${displayed text}=    Get Text    ${locators}[login failed message]
    Should Be Equal As Strings    ${displayed text}    ${expected login failed message}
the page shall display the ${element}
    Page Should Contain Element    ${locators}[${element}]
the user types an appointment date
    Input Text    ${locators}[visit date selector]    ${appointment date}
    