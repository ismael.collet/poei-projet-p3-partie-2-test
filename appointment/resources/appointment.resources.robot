*** Settings ***
Documentation     Ressources de appointment.robot
Library    SeleniumLibrary
*** Variables ***
${website url}    https://katalon-demo-cura.herokuapp.com/
&{urls}    home=${website url}
...    login=${website url}profile.php#login
...    appointment=${website url}#appointment
...    appointment confirmation=${website url}appointment.php#summary
${browser}    firefox
&{username}    valid=John Doe    wrong=AAA BBB
&{password}    valid=ThisIsNotAPassword    wrong=AAA
${expected login failed message}    Login failed! Please ensure the username and password are valid.
${appointment date}    01/01/2030
&{locators}    "Make Appointment"=id:btn-make-appointment
...    "Login"=id:btn-login
...    username=id:txt-username
...    password=id:txt-password
...    login failed message=//p[@class='lead text-danger']
...    Appointment page title=//div[@class="col-sm-12 text-center"]/h2
...    facility selection scrolling menu=id:combo_facility
...    apply for readmission checkbox=id:chk_hospotal_readmission
...    medicare radio button=id:radio_program_medicare
...    medicaid radio button=id:radio_program_medicaid
...    none radio button=id:radio_program_none
...    visit date selector=id:txt_visit_date
...    comment text box=id:txt_comment
...    "Book Appointment"=id:btn-book-appointment
...    book appointment button=id:btn-book-appointment
...    the lateral page toggle button=id:menu-toggle
...    "Logout"=//li[a="Logout"]/a