*** Settings ***
Documentation     Locators de dashboard.robot
*** Variables ***
# Login page
${locator - username box}    name:username
${locator - password box}    name:password
${locator - login button}    class:orangehrm-login-button
# Dashboard page
${locator - side panel menu}    //ul[@class="oxd-main-menu"]
${locator - all tools}    ${locator - side panel menu}/li

*** Keywords ***
locator of ${n}th tool's text
    [return]    //ul[@class="oxd-main-menu"]/li[${n}]/a/span
