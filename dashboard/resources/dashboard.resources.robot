*** Settings ***
Documentation     Ressources de dashboard.robot
Library    SeleniumLibrary
*** Variables ***
${url - homepage}    https://opensource-demo.orangehrmlive.com/web/index.php/auth/login
${browser}    firefox
${valid username}    Admin
${valid password}    admin123
@{expected tools}    Admin    PIM    Leave    Time    Recruitment    My Info    Performance    
...    Dashboard    Directory    Maintenance    Buzz
