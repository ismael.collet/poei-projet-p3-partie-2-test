*** Settings ***
Documentation     Tests des fonctionalités du dashboard de opensource-demo.orangehrmlive.com
Library    SeleniumLibrary
Resource    resources/dashboard.resources.robot
Resource    resources/dashboard.locators.robot
Test Setup    Load the dashboard
Test Teardown    Close Browser

*** Test Cases ***
Tool displaying
    The number of displayed tools shall be equal to the number of expected tools
    And all expected tools shall be displayed in the side panel menu

*** Keywords ***
Load the dashboard
    Open Browser    ${url - homepage}    ${browser}
    Wait Until Page Contains Element  ${locator - login button}
    Input text    ${locator - username box}    ${valid username}
    Input text    ${locator - password box}    ${valid password}
    Click Button    ${locator - login button}
    Wait Until Page Contains Element    ${locator - side panel menu}
The number of displayed tools shall be equal to the number of expected tools
    ${nbr of expected tools}    Get Length    ${expected tools}
    ${nbr of displayed tools}    Get Element Count   ${locator - all tools}
    Should Be Equal As Integers    ${nbr of displayed tools}    ${nbr of expected tools}
All expected tools shall be displayed in the side panel menu
    ${i}    Evaluate    1
    FOR    ${expected tool's name}    IN    @{expected tools}
        ${displayed tool's locator}    locator of ${i}th tool's text
        ${displayed tool's name}    Get Text    ${displayed tool's locator}
        Should Be Equal As Strings    ${displayed tool's name}    ${expected tool's name}
        ${i}    Evaluate    ${i}+1
    END
